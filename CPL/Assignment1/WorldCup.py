import random as random


f = open("Qualified teams.txt", "r")
f.readline()
teams =[] 
for i in range(32):
    team = f.readline()
    team = team.split(',')
    team = ( team[0] , int(team[1]) )
    teams.append( team )
f.close()

teams = sorted( teams , key=lambda x: x[1] , reverse=True )


rGroups= []

rGroups.append(teams[:7])
rGroups[0].append(teams[ len(teams)-1 ])
rGroups.append(teams[7:15])
rGroups.append(teams[15:23])
rGroups.append(teams[23:31])
groups=[]

for i in range( 8 ):
    group = []
    while( len(group) < 4 ):
        for j in range(4):
            team = random.choice(rGroups[j])
            rGroups[j].remove(team)
            group.append(team)
    groups.append(group)


for i in range(8):
    print(groups[i])
    print('\n')
