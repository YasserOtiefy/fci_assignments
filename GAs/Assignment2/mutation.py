import random
#----------------------------------------------------------
def mutate(chromosome, t):
    non_uniform_mutation(chromosome, -10, 10, t, 200, 5)
#----------------------------------------------------------
def non_uniform_mutation(chromosome, LB, UB, t, T, b):
    for i in range(len(chromosome)):
        xi = chromosome[i]
        delta_LB_xi = max(LB, xi - LB)
        delta_UB_xi = min(UB, UB - xi)
        r = random.random()
        y = delta_LB_xi if r <= 0.5 else delta_UB_xi
        delta_xi_t = y * (1 - (r ** ((1 + (t/T)) ** b)))
        if delta_xi_t < LB:
            delta_xi_t = LB
        elif delta_xi_t > UB:
            delta_xi_t = UB
        chromosome[i] = delta_xi_t
#----------------------------------------------------------
