import random as random
import mutation
#--------------------------------------------------------------------------------------
def f(x, individual):
    yCalc = 0.0
    for i in range(len(individual)):
        yCalc += (x ** i) * individual[i]
    return yCalc
#--------------------------------------------------------------------------------------
def get_MSError(points, chromosome):
    sum_MSError = 0.0
    for point in points:
        sum_MSError += ((f(point.x, chromosome) - point.y) ** 2)
    return sum_MSError / len(points)
#--------------------------------------------------------------------------------------
def select_chromosome(size, points , GEN_MAX = 200 , POP_SIZE = 70):
#--------------------------------------------------------------------------------------
    def fitness(individual):
        #Compute the fitness of individual
        sumSquare = 0.0
        for point in points:
            difference_square = (f(point.x, individual) - point.y) ** 2
            sumSquare += difference_square
        sumSquare = sumSquare / len(points)
        return sumSquare
#--------------------------------------------------------------------------------------
    def generate_starting_population(POP_SIZE):
        return [generate_individual(-10.0, 10.0) for x in range (0,POP_SIZE)]
#--------------------------------------------------------------------------------------
    def generate_individual(LB, UB):
        splitter = size.split(' ')
        return [random.uniform(LB, UB) for x in range (0, int(splitter[1]) + 1)]
# -----------------------------------------------------------------------------------------------------------------------------
    def GA(population, current_generation,  prop_of_crossover = 0.2, prop_of_mutation = 0.08, prop_of_survival = 0.05, b = 0.5):
        parent_length = int(prop_of_crossover*len(population))
        survivals = population[len(population) - parent_length:]
        nonparents = population[: len(population) - parent_length]
        # Parent selection!
        for np in nonparents:
            if prop_of_survival > random.random():
                survivals.append(np)
        # parents mutation
        for p in survivals:
            if prop_of_mutation > random.random():
                mutation.mutate(p, current_generation)
        # Produce offsprings
        offsprings = []
        desired_length = len(population) - len(survivals)
        while len(offsprings) < desired_length :
            parent1 = population[random.randint(0,len(survivals)-1)]
            parent2 = population[random.randint(0,len(survivals)-1)]        
            half = random.randint(0,len(parent1)-1)
            offspring1 = parent1[:int(half)] + parent2[int(half):] #crossover
            offspring2 = parent2[:int(half)] + parent1[int(half):] #crossover
            if prop_of_mutation > random.random():
                mutation.mutate(offspring1, current_generation)
                mutation.mutate(offspring2, current_generation)
            offsprings.append(offspring1)
            offsprings.append(offspring2)
        new_population = survivals
        new_population.extend(offsprings)
        return new_population
    # ----------------------------------------------------------------------------------------------------------------
    generation = 1
    population = generate_starting_population(POP_SIZE)
    for g in range(0,GEN_MAX):
        population = GA(population, generation)
        population = sorted(population, key=lambda x: fitness(x), reverse=False)
        generation += 1
    print(len(population))
    return population[0]
#-------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------
